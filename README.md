## Integration:
In order to publicly serve the ai-audit page, an entry has to be added on the nginx configuration. A sample entry is also included in [./ai-audit.nginx.conf](./ai-audit.nginx.conf)
## Usage
A .gitignored .env.eunomia is required for the included [./docker-compose-ml-feedback.yml](./docker-compose-ml-feedback.yml) example configuration file.
An [./env.eunomia.sample](./env.eunomia.sample) can be used as template, with the names and the ports of the rest of the services that are to be included (used with the relevant configuration files).
When used with nginx and the rest of the required services, one can visit the https://${DOMAIN_NAME}/audit and review / submit feedback for the relevant ai services. A simple script to get the generated (or to send if needed) csv files with the users' feedback is included in [./docker/scripts/csvs.sh](./docker/scripts/csvs.sh).
