#!/bin/sh

set -e

_CWD="$(pwd)"
_HERE="$(dirname "$(readlink -f "$0")")"
if [ "${_HERE}" = "." ];then
  _HERE="$(pwd)"
fi

cd "${_HERE}" || exit

retrieve() {
  docker cp eunomia-ml-feedback:/app/static/paraphrase.csv "${_HERE}/app/static/paraphrase.csv" > /dev/null 2>&1 || :
  docker cp eunomia-ml-feedback:/app/static/stance.csv "${_HERE}/app/static/stance.csv" > /dev/null 2>&1 || :
  docker cp eunomia-ml-feedback:/app/static/sentiment.csv "${_HERE}/app/static/sentiment.csv" > /dev/null 2>&1 || :
  docker cp eunomia-ml-feedback:/app/static/subjectivity.csv "${_HERE}/app/static/subjectivity.csv" > /dev/null 2>&1 || :
}

send() {
  if [ -f "${_HERE}/app/static/subjectivity.csv" ];then
    docker cp "${_HERE}/app/static/subjectivity.csv" eunomia-ml-feedback:/app/static/subjectivity.csv > /dev/null 2>&1 || :
  fi
  if [ -f "${_HERE}/app/static/sentiment.csv" ];then
    docker cp "${_HERE}/app/static/sentiment.csv" eunomia-ml-feedback:/app/static/subjectivity.csv > /dev/null 2>&1 || :
  fi
  if [ -f "${_HERE}/app/static/stance.csv" ];then
    docker cp "${_HERE}/app/static/stance.csv" eunomia-ml-feedback:/app/static/subjectivity.csv > /dev/null 2>&1 || :
  fi
  if [ -f "${_HERE}/app/static/paraphrase.csv" ];then
    docker cp "${_HERE}/app/static/paraphrase.csv" eunomia-ml-feedback:/app/static/subjectivity.csv > /dev/null 2>&1 || :
  fi
}

if [ "${1}" = "--send" ];then
  send
elif [ "${1}" = "--get" ];then
  retrieve
fi

if [ ! "$(pwd)" = "${_CWD}" ]; then
  cd "${_CWD}" || exit
fi
