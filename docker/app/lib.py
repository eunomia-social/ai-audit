import os
import csv
import requests
from typing import Any, Optional

from .models import ActionType, CsvFileEntry, ACTION_OPTIONS  # noqa

PREFIX = "/audit"
STATIC_PATH = os.path.realpath(os.path.join(os.path.dirname(__file__), "static"))
TEMPLATES_PATH = os.path.realpath(os.path.join(os.path.dirname(__file__), "templates"))

PARAPHRASE_THRESHOLD = 0.5
# old (everything managed from esn-gateway)
ESN_GATEWAY_HOST = os.environ.get("ESN_GATEWAY_ADDRESS", "172.23.0.7")
ESN_GATEWAY_PORT = os.environ.get("ESN_GATEWAY_PORT", "5054")
FALLBACK_URL = f"http://{ESN_GATEWAY_HOST}:{ESN_GATEWAY_PORT}/audit"
# new (one container per service)
REMOTE_SERVERS = {
    ActionType.paraphrase: {
        "host": "TEXT_SIMILARITY_ADDRESS",
        "port": "TEXT_SIMILARITY_PORT",
    },
    ActionType.stance: {
        "host": "STANCE_DETECTION_ADDRESS",
        "port": "STANCE_DETECTION_PORT",
    },
    ActionType.sentiment: {
        "host": "SENTIMENT_ADDRESS",
        "port": "SENTIMENT_PORT"
    },
    ActionType.subjectivity: {
        "host": "SUBJECTIVITY_ADDRESS",
        "port": "SUBJECTIVITY_PORT"
    },
}
REMOTE_ENDPOINT_PATHS = {
    ActionType.paraphrase: "/text_similarity",
    ActionType.stance: "/stance_detection",
    ActionType.sentiment: "/sentiment",
    ActionType.subjectivity: "/subjectivity",
}

SINGLE_TEXT = ["sentiment", "subjectivity"]


def _csv_filed_names(csv_name: str):
    number_of_texts = 2 if csv_name not in SINGLE_TEXT else 1
    fieldnames = []
    if number_of_texts == 2:
        fieldnames.extend(["text_a", "text_b"])
    else:
        fieldnames.append("text")
    fieldnames.extend(["result", "correct", "comment"])
    return fieldnames


def _get_results_request(url, body: dict) -> Any:
    response = requests.post(url=url, json=body, timeout=60)
    if response.status_code < 300:
        result = response.json()
        return result.get("result", None)
    print(response.text)
    return None


def _get_remote_endpoint(action_type: ActionType) -> str:
    url = f"{FALLBACK_URL}{REMOTE_ENDPOINT_PATHS[action_type]}"
    remote_host = os.environ.get(REMOTE_SERVERS[action_type]["host"], None)
    remote_port = os.environ.get(REMOTE_SERVERS[action_type]["port"], None)
    if remote_host is not None and remote_port is not None:
        return f"http://{remote_host}:{remote_port}{REMOTE_ENDPOINT_PATHS[action_type]}"
    return url


def _get_paraphrase(text1: str, text2: str) -> Optional[str]:
    url = _get_remote_endpoint(ActionType.paraphrase)
    result = _get_results_request(url=url, body={"similar_posts": [[text1, text2]]})
    if result is not None:
        print(result)
        _similarity = float(result)
        if _similarity < PARAPHRASE_THRESHOLD:
            return "Not Paraphrased"
        return "Paraphrased"
    return None


def _get_stance(text1: str, text2: str) -> Optional[str]:
    url = _get_remote_endpoint(ActionType.stance)
    result = _get_results_request(url=url, body={"text1": text1, "text2": text2})
    if result is not None:
        return str(result).capitalize()
    return None


def _get_sentiment(text: str) -> Optional[str]:
    url = _get_remote_endpoint(ActionType.sentiment)
    result = _get_results_request(url=url, body={"text": text})
    if result is not None:
        # [-1,0,1] (negative, neutral, positive)
        sentiment_class = result[0]
        return (
            "Negative"
            if sentiment_class < 0
            else "Positive"
            if sentiment_class > 0
            else "Neutral"
        )
    return None


def _get_subjectivity(text: str) -> Optional[str]:
    url = _get_remote_endpoint(ActionType.subjectivity)
    result = _get_results_request(url=url, body={"text": text})
    if result is not None:
        _confidence = float(result)
        return "Subjective" if _confidence > 0.5 else "Objective"
    return None


def csv_head(csv_name: str):
    csv_file = os.path.join(f"{STATIC_PATH}/{csv_name}.csv")
    if not os.path.exists(csv_file):
        with open(csv_file, "w", newline="") as _file:
            writer = csv.DictWriter(
                _file, fieldnames=_csv_filed_names(csv_name=csv_name)
            )
            writer.writeheader()


def csv_append(csv_name: str, csv_values: CsvFileEntry):
    csv_head(csv_name=csv_name)
    csv_file = os.path.join(f"{STATIC_PATH}/{csv_name}.csv")
    with open(csv_file, "a", newline="") as _file:
        writer = csv.DictWriter(_file, fieldnames=_csv_filed_names(csv_name=csv_name))
        writer.writerow(csv_values.dict(exclude_none=True))


def get_ml_result(action: ActionType, text1: str, text2: str = "") -> Optional[str]:
    if action == ActionType.paraphrase:
        return _get_paraphrase(text1=text1, text2=text2)
    elif action == ActionType.stance:
        return _get_stance(text1=text1, text2=text2)
    elif action == ActionType.sentiment:
        return _get_sentiment(text=text1)
    elif action == ActionType.subjectivity:
        return _get_subjectivity(text=text1)
    else:
        return None
