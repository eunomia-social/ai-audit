from enum import Enum
from pydantic import BaseModel, Field, root_validator
from typing import Optional


class ActionType(str, Enum):
    paraphrase = "paraphrase"
    stance = "stance"
    sentiment = "sentiment"
    subjectivity = "subjectivity"


ACTION_OPTIONS = {
    ActionType.paraphrase: ["Paraphrased", "Not Paraphrased"],
    ActionType.stance: ["Support", "Deny", "Comment", "Query"],
    ActionType.sentiment: ["Positive", "Neutral", "Negative"],
    ActionType.subjectivity: ["Subjective", "Objective"],
}


class CsvFileEntry(BaseModel):
    result: str
    correct: str
    comment: str
    text: Optional[str] = None
    text_a: Optional[str] = None
    text_b: Optional[str] = None


class CheckBody(BaseModel):
    action: ActionType
    text1: str = Field(default="", min_length=1)
    text2: Optional[str] = Field(default=None, min_length=1)

    @root_validator(pre=True)
    def check_action_and_texts(cls, values):
        """If the action is paraphrase or stance we need two text fields."""
        _action = values.get("action", None)
        if _action is None:
            raise ValueError("action is a required field")
        if _action in [ActionType.paraphrase, ActionType.stance] and not values.get(
            "text2", None
        ):
            raise ValueError("text2 is a required field")
        return values


class SubmitBody(CheckBody):
    result: str
    correct: str
    comment: str = Field(default="", min_length=0)

    @root_validator(pre=True)
    def check_valid_result(cls, values):
        """Make sure the submitted result is one of the expected ones"""
        action: ActionType = values.get("action", None)
        if action is None:
            raise ValueError("action is a required field")
        result = values.get("result", None)
        correct = values.get("correct", None)
        if result is None:
            raise ValueError("result is a required field")
        if correct is None:
            raise ValueError("correct is a required field")
        if correct not in ACTION_OPTIONS[action]:
            raise ValueError("correct: is an unaccepted value")
        if result not in ACTION_OPTIONS[action]:
            raise ValueError("result: is an unaccepted value")
        return values
