const checkboxEl = document.getElementById('correct-result');
const selectEl = document.getElementById('select-correct-value');
const submitEl = document.getElementById('submit-feedback');
const checkBtnEl =  document.getElementById('check-input-button');
const textAreaEls = document.getElementsByTagName('textarea');
const commentAreaEl = document.getElementById('suggestion-comment');
const resultEl = document.getElementById('result-value');
const actionType = document.getElementById('action-type');

function onTextAreaChange(_) {
    let canCheck = true;
    for (let index = 0; index < textAreaEls.length; index++) {
        const textArea = textAreaEls[index];
        if (textArea.value.length === 0 && textArea.id !== 'suggestion-comment') {
            canCheck = false;
            break
        }
    }
    checkBtnEl.disabled = !canCheck;
}

function onCheckboxChange(_) {

    selectEl.disabled = !checkboxEl.checked;
    commentAreaEl.disabled = !checkboxEl.checked;
    submitEl.disabled = !checkboxEl.checked;
}

function onSelectionChange(_) {
    submitEl.disabled = selectEl.selectedIndex === 0;
}

function doCheck(body) {
    fetch(`${apiPrefix}/check`, { method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body
    }).then(function (response) {
        if (response.status !== 200) {
            console.log(response.status);
            return;
        }
        response.json().then(function (answer) {
            const { result } = answer;
            resultEl.value = result[0].toUpperCase() + result.slice(1);
            checkboxEl.disabled = false;
        });
    }).catch(function(err) {
        console.log(err);
    });
}

function getFetchBoy(){
    if (actionType.value === 'paraphrase' || actionType.value === 'stance') {
        let text1 = document.getElementById('input-text-a').value;
        let text2 = document.getElementById('input-text-b').value;
        if (text1.length > 0 && text2.length > 0) {
            return {action: actionType.value, text1, text2 };
        }
    } else if (actionType.value === 'sentiment' || actionType.value === 'subjectivity') {
        let text1 = document.getElementById('input-text').value;
        if (text1.length >0) {
            return {action: actionType.value, text1 };
        }
    }
    return null;
}

function onCheck(_) {
    resultEl.value = '';
    checkboxEl.disabled = true;
    selectEl.disabled = true;
    selectEl.selectedIndex = 0;
    const checkBody = getFetchBoy();
    if (checkBody !== null) {
        doCheck(JSON.stringify(checkBody));
    }
}

function onSubmitFeedback(_) {
    if (!submitEl.disabled) {
        const body = getFetchBoy();
        const result = resultEl.value;
        let correct = result;
        if (selectEl.selectedIndex > 0) {
            correct = selectEl.options[selectEl.selectedIndex].value;
        }
        if (body !== null && result.length > 0) {
            body.result = result;
            body.correct = correct;
            body.comment = commentAreaEl.value;
            submitEl.disabled = true;
            fetch(`${apiPrefix}/submit`, {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(body),
            }).then(function (response) {
                if (response.status > 300) {
                    console.log(response.status);
                }
            }).catch(function (err) {
                console.log(err);
            }).finally(() => {
                let textAreas = document.getElementsByTagName('textarea');
                for (let index = 0; index < textAreas.length; index++) {
                    textAreas[index].value = '';
                }
                selectEl.disabled = true;
                selectEl.selectedIndex = 0;
                selectEl.value = selectEl.options[0];
                resultEl.value = '';
                commentAreaEl.disabled = true;
                checkboxEl.disabled = true;
                checkBtnEl.disabled = true;
            });
        }
    }
}

function setListeners(setOn) {
    let textAreas = document.getElementsByTagName('textarea');
    for (let index = 0; index < textAreas.length; index++) {
        if (setOn) {
            textAreas[index].addEventListener('input', onTextAreaChange);
        } else {
            textAreas[index].removeEventListener('input', onTextAreaChange);
        }
    }
    if (setOn) {
        checkBtnEl.addEventListener('click', onCheck);
        submitEl.addEventListener('click', onSubmitFeedback);
        checkboxEl.addEventListener('click', onCheckboxChange);
        selectEl.addEventListener('change', onSelectionChange);
    } else {
        checkBtnEl.removeEventListener('click', onCheck);
        submitEl.removeEventListener('click', onSubmitFeedback);
        checkboxEl.removeEventListener('click', onCheckboxChange);
        selectEl.removeEventListener('change', onSelectionChange);
    }
}


function onLoad(_) {
    setListeners(true);
}

function onUnload(_) {
    setListeners(false);
}

window.addEventListener('load', onLoad);
window.addEventListener('unload', onUnload);
