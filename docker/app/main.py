from __future__ import annotations
from pydantic import ValidationError
from fastapi.staticfiles import StaticFiles
from fastapi import FastAPI, Request
from starlette.responses import RedirectResponse
from starlette.templating import Jinja2Templates

from .models import CheckBody, SubmitBody, ACTION_OPTIONS  # noqa
from .lib import (  # noqa
    PREFIX,
    TEMPLATES_PATH,
    STATIC_PATH,
    SINGLE_TEXT,
    ActionType,
    CsvFileEntry,
    csv_head,
    csv_append,
    get_ml_result,
)

app = FastAPI(redoc_url=None, docs_url=None)
templates = Jinja2Templates(directory=TEMPLATES_PATH)
app.mount(f"{PREFIX}/static", StaticFiles(directory=STATIC_PATH), name="static")


@app.on_event("startup")
async def startup_event():
    for csv_name in ACTION_OPTIONS.keys():
        csv_head(csv_name=csv_name.value)


@app.head("/health", include_in_schema=False, status_code=204)
async def head_check():  # pragma: no cover
    """Health check"""
    return None


@app.get("/")
async def root():
    return RedirectResponse(url=f"{PREFIX}")


@app.get(f"{PREFIX}")
@app.get(f"{PREFIX}/")
def main_view(request: Request):
    return templates.TemplateResponse(
        "index.html",
        {"request": request, "active": "", "options": [], "prefix": PREFIX},
    )


@app.post(f"{PREFIX}/check")
@app.post(f"{PREFIX}/check/")
async def on_check(body: CheckBody):
    return {
        "result": get_ml_result(action=body.action, text1=body.text1, text2=body.text2)
    }


@app.post(f"{PREFIX}/submit", status_code=204)
@app.post(f"{PREFIX}/submit/", status_code=204)
async def on_submit(body: SubmitBody):
    try:
        entry = CsvFileEntry(
            result=body.result, correct=body.correct, comment=body.comment
        )
        if body.action.value in SINGLE_TEXT:
            entry.text = body.text1
        else:
            entry.text_a = body.text1
            entry.text_b = body.text2
        csv_append(body.action.value, entry)
    except ValidationError as e:
        raise e
    return None


@app.get(PREFIX + "/{subview}")
@app.get(PREFIX + "/{subview}")
def paraphrase_view(request: Request, subview: ActionType):
    return templates.TemplateResponse(
        "index.html",
        {
            "request": request,
            "active": subview.value,
            "options": ACTION_OPTIONS[subview],
            "prefix": PREFIX,
        },
    )
